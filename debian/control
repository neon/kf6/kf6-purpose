Source: kf6-purpose
Section: libs
Priority: optional
Maintainer: Jonathan Esk-Riddell <jr@jriddell.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               intltool,
               kaccounts-integration-dev,
               kf6-extra-cmake-modules,
               kf6-kcmutils-dev,
               kf6-kconfig-dev,
               kf6-kcoreaddons-dev,
               kf6-kdeclarative-dev,
               kf6-ki18n-dev,
               kf6-kio-dev,
               kf6-kirigami2-dev,
               kf6-knotifications-dev,
               kf6-kwallet-dev,
               libaccounts-glib-dev,
               pkgconf,
               pkg-kde-tools-neon,
               qt6-base-dev,
               qt6-declarative-dev
Standards-Version: 4.6.2
Homepage: https://cgit.kde.org/purpose.git/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/purpose
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/purpose.git

Package: kf6-purpose
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: kf6-bluez-qt,
         kf6-kcmutils,
         kf6-kirigami2,
         kf6-prison,
         qml6-module-sso-onlineaccounts,
         qt6-5compat,
         qt6-base,
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: libkf5purpose-bin (<< 5.116.0),
Replaces: libkf5purpose-bin (<< 5.116.0),
          libkf6purpose6,
          libkf6purpose-bin,
          libkf6purpose-data,
          qml6-module-org-kde-purpose,
Description: abstraction to provide and leverage actions of a specific kind, runtime
 Purpose offers the possibility to create integrate services and actions on
 any application without having to implement them specifically. Purpose will
 offer them mechanisms to list the different alternatives to execute given the
 requested action type and will facilitate components so that all the plugins
 can receive all the information they need.
 .
 This package contains the Purpose runtime elements.

Package: kf6-purpose-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: kf6-kcoreaddons-dev,
         kf6-purpose (= ${binary:Version}),
         qt6-base-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: libkf6purpose-dev,
          libkf6purpose-doc,
Description: abstraction to provide and leverage actions of a specific kind, devel files
 Purpose offers the possibility to create integrate services and actions on
 any application without having to implement them specifically. Purpose will
 offer them mechanisms to list the different alternatives to execute given the
 requested action type and will facilitate components so that all the plugins
 can receive all the information they need.
 .
 This package contains the Purpose development files.

Package: libkf6purpose6
Architecture: all
Depends: kf6-purpose, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6purpose-bin
Architecture: all
Depends: kf6-purpose, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6purpose-data
Architecture: all
Depends: kf6-purpose, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6purpose-dev
Architecture: all
Depends: kf6-purpose-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6purpose-doc
Architecture: all
Depends: kf6-purpose-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-org-kde-purpose
Architecture: all
Depends: kf6-purpose, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.


